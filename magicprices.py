import requests
import pandas as pd


def get_prices_query(search_query: str) -> list:
    """
    Gets list of card data from search query

    :param search_query: Card name string
    :return: List of card data
    """
    # Setup search paramters, "q" = query, "order" = sort order
    params = {
        "q": search_query,
        "order": "usd"
    }

    # Store the HTTP response in "response" variable
    response = requests.get("https://api.scryfall.com/cards/search", params=params)

    # Return the list "data" from the response
    return response.json()["data"]


def get_prices_table(search_query: str) -> str:
    """
    Gets a table string of card prices from search query

    :param search_query: Card name string
    :return: Table string of card prices
    """
    # Setup columns for DataFrame
    data = {
        "Card name": [],
        "Price (USD)": []
    }

    # Store prices data from HTTP API request into "prices_data_raw" variable
    prices_data_raw = get_prices_query(search_query)

    # Add the data for each card into the "data" dictionary's arrays
    for card in prices_data_raw:
        # Insert the data in reverse order, otherwise highest price cards would be first
        data["Card name"].insert(0, card["name"])
        data["Price (USD)"].insert(0, card["prices"]["usd"])

    # Return table string
    return pd.DataFrame(data).to_string(index=False)


def search_card_prompt():
    """
    Prompts the user to search for cards, prints table of matching cards and prices

    :return: None
    """
    # Store user's input into "user_input" variable
    user_input = input("Enter card name to search: ")

    # Get prices table string from user's input query
    prices_table = get_prices_table(user_input)

    # Print results for the query
    print(prices_table)


def main():
    # Run forever
    while True:
        search_card_prompt()


# Run only when file is run directly
if __name__ == "__main__":
    main()
